import pandas as pd
from sklearn.model_selection import train_test_split, cross_val_score
from sklearn.naive_bayes import GaussianNB
from sklearn.metrics import accuracy_score, classification_report, precision_score, recall_score, f1_score, \
    confusion_matrix, ConfusionMatrixDisplay
from sklearn.preprocessing import MinMaxScaler

# Load the dataset
data = pd.read_csv('../COVID-19_Symptoms_Checker_Dataset/Cleaned-Data.csv')

# Define the columns to be ignored
columns_to_ignore = ['Age_0-9', 'Age_10-19', 'Age_20-24', 'Age_25-59', 'Age_60+',
                     'Gender_Female', 'Gender_Male', 'Gender_Transgender',
                     'Contact_Dont-Know', 'Contact_No', 'Contact_Yes']

# Drop the ignored columns from the data
data = data.drop(columns_to_ignore, axis=1)


X = data.drop(['Severity_Severe', 'Severity_None'], axis=1)  
X = pd.get_dummies(X)  
y = data[['Severity_Severe', 'Severity_None']]  


X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)


scaler = MinMaxScaler()
X_train_normalized = scaler.fit_transform(X_train)
X_test_normalized = scaler.transform(X_test)


naive_bayes_severe = GaussianNB()
naive_bayes_none = GaussianNB()
naive_bayes_severe.fit(X_train_normalized, y_train['Severity_Severe'])
naive_bayes_none.fit(X_train_normalized, y_train['Severity_None'])


y_pred_severe = naive_bayes_severe.predict(X_test_normalized)
y_pred_none = naive_bayes_none.predict(X_test_normalized)


accuracy_severe = accuracy_score(y_test['Severity_Severe'], y_pred_severe)
accuracy_none = accuracy_score(y_test['Severity_None'], y_pred_none)
print(f'Accuracy (Severity_Severe): {accuracy_severe}')
print(f'Accuracy (Severity_None): {accuracy_none}')


scores_severe = cross_val_score(naive_bayes_severe, X, y['Severity_Severe'], cv=5)  
mean_accuracy_severe = scores_severe.mean()
scores_none = cross_val_score(naive_bayes_none, X, y['Severity_None'], cv=5)  
mean_accuracy_none = scores_none.mean()
print(f'Mean Accuracy (Severity_Severe, Cross-validation): {mean_accuracy_severe}')
print(f'Mean Accuracy (Severity_None, Cross-validation): {mean_accuracy_none}')


print('Classification Report (Severity_Severe):')
print(classification_report(y_test['Severity_Severe'], y_pred_severe))
print('Classification Report (Severity_None):')
print(classification_report(y_test['Severity_None'], y_pred_none))



import matplotlib.pyplot as plt


precision_severe = precision_score(y_test['Severity_Severe'], y_pred_severe, average='binary', pos_label=1)
recall_severe = recall_score(y_test['Severity_Severe'], y_pred_severe, average='binary', pos_label=1)
f1_severe = f1_score(y_test['Severity_Severe'], y_pred_severe, average='binary', pos_label=1)


precision_none = precision_score(y_test['Severity_None'], y_pred_none, average='binary', pos_label=1)
recall_none = recall_score(y_test['Severity_None'], y_pred_none, average='binary', pos_label=1)
f1_none = f1_score(y_test['Severity_None'], y_pred_none, average='binary', pos_label=1)

# partie de Création des graphiques
fig, axs = plt.subplots(2, 3, figsize=(12, 8))
fig.suptitle('Performance Metrics for Each Model')


axs[0, 0].bar(['Severity_Severe', 'Severity_None'], [accuracy_severe, accuracy_none])
axs[0, 0].set_title('Accuracy')
axs[0, 0].set_ylabel('Score')


axs[0, 1].bar(['Severity_Severe', 'Severity_None'], [precision_severe, precision_none])
axs[0, 1].set_title('Precision')
axs[0, 1].set_ylabel('Score')


axs[0, 2].bar(['Severity_Severe', 'Severity_None'], [recall_severe, recall_none])
axs[0, 2].set_title('Recall')
axs[0, 2].set_ylabel('Score')


axs[1, 0].bar(['Severity_Severe', 'Severity_None'], [f1_severe, f1_none])
axs[1, 0].set_title('F1-score')
axs[1, 0].set_ylabel('Score')


cm_severe = confusion_matrix(y_test['Severity_Severe'], y_pred_severe)
disp_severe = ConfusionMatrixDisplay(confusion_matrix=cm_severe, display_labels=['Severity_Severe', 'Not Severity_Severe'])
disp_severe.plot(ax=axs[1, 1], cmap='Blues', colorbar=False)
axs[1, 1].set_title('Confusion Matrix (Severity_Severe)')


cm_none = confusion_matrix(y_test['Severity_None'], y_pred_none)
disp_none = ConfusionMatrixDisplay(confusion_matrix=cm_none, display_labels=['Severity_None', 'Not Severity_None'])
disp_none.plot(ax=axs[1, 2], cmap='Blues', colorbar=False)
axs[1, 2].set_title('Confusion Matrix (Severity_None)')


plt.tight_layout()

# Affichage des graphiques
plt.show()

