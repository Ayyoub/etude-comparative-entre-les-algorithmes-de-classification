import pandas as pd
from sklearn.model_selection import train_test_split, cross_val_score
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import accuracy_score
from sklearn.preprocessing import MinMaxScaler


data = pd.read_csv('../COVID-19_Symptoms_Checker_Dataset/Cleaned-Data.csv')


columns_to_ignore = ['Age_0-9', 'Age_10-19', 'Age_20-24', 'Age_25-59', 'Age_60+',
                     'Gender_Female', 'Gender_Male', 'Gender_Transgender',
                     'Contact_Dont-Know', 'Contact_No', 'Contact_Yes']


data = data.drop(columns_to_ignore, axis=1)


X = data.drop(['Severity_Severe'], axis=1)
X = pd.get_dummies(X)
y = data['Severity_Severe']


X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)


scaler = MinMaxScaler()
X_train_normalized = scaler.fit_transform(X_train)
X_test_normalized = scaler.transform(X_test)


logreg = LogisticRegression()
logreg.fit(X_train_normalized, y_train)


y_pred = logreg.predict(X_test_normalized)


accuracy = accuracy_score(y_test, y_pred)
print(f'Accuracy: {accuracy}')


scores = cross_val_score(logreg, X, y, cv=5)
mean_accuracy = scores.mean()
print(f'Mean Accuracy (Cross-validation): {mean_accuracy}')
