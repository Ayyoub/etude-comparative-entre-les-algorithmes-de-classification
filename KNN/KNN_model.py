import pandas as pd
from sklearn.model_selection import train_test_split
from sklearn.neighbors import KNeighborsClassifier
from sklearn.metrics import accuracy_score
from sklearn.model_selection import GridSearchCV
import pickle

data = pd.read_csv('../COVID-19_Symptoms_Checker_Dataset/Cleaned-Data.csv')

columns_to_ignore = ['Age_0-9', 'Age_10-19', 'Age_20-24', 'Age_25-59', 'Age_60+', 'Gender_Female', 'Gender_Male', 'Gender_Transgender', 'Contact_Dont-Know', 'Contact_No', 'Contact_Yes']

data = data.drop(columns_to_ignore, axis=1)

X = data.drop(['Severity_None', 'Severity_Severe'], axis=1)  # Caractéristiques d'entrée
X = pd.get_dummies(X)
y = data[['Severity_None', 'Severity_Severe']]

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)

param_grid = {'n_neighbors': [3, 5, 7, 9]}

grid_search = GridSearchCV(KNeighborsClassifier(), param_grid, cv=5)
grid_search.fit(X_train, y_train)

best_params = grid_search.best_params_
best_score = grid_search.best_score_
print(f'Best Parameters: {best_params}')
print(f'Best Score: {best_score}')

knn = KNeighborsClassifier(n_neighbors=best_params['n_neighbors'])
knn.fit(X_train, y_train)

y_pred = knn.predict(X_test)

accuracy = accuracy_score(y_test, y_pred)
print(f'Accuracy: {accuracy}')


correct_predictions = np.sum((y_pred == y_test).all(axis=1))

class_counts = np.sum(y_pred, axis=0)

class_counts = np.sum(y_pred, axis=0)

# Création du graphique à barres
labels = ['Severity_None', 'Severity_Severe']
x = np.arange(len(labels))

plt.bar(x, class_counts)
plt.xticks(x, labels)
plt.ylabel('Number of Predictions')
plt.title('Number of Predictions by Class')
plt.show()