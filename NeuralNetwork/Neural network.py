import numpy as np
import pandas as pd
from matplotlib import pyplot as plt
from sklearn.model_selection import train_test_split, cross_val_score
from sklearn.neural_network import MLPClassifier
from sklearn.metrics import accuracy_score
from sklearn.preprocessing import MinMaxScaler
from sklearn.multioutput import MultiOutputClassifier

data = pd.read_csv('../COVID-19_Symptoms_Checker_Dataset/Cleaned-Data.csv')

X = data.drop(['Severity_None', 'Severity_Severe'], axis=1)
X = pd.get_dummies(X)
y = data[['Severity_None', 'Severity_Severe']]  

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)




from sklearn.preprocessing import StandardScaler

scaler = StandardScaler()
X_train_normalized = scaler.fit_transform(X_train)
X_test_normalized = scaler.transform(X_test)

mlp = MLPClassifier()
multi_mlp = MultiOutputClassifier(mlp)

multi_mlp.fit(X_train_normalized, y_train)

y_pred = multi_mlp.predict(X_test_normalized)

accuracy = accuracy_score(y_test, y_pred)
print(f'Accuracy: {accuracy}')

scores = cross_val_score(multi_mlp, X, y, cv=5)
mean_accuracy = scores.mean()
print(f'Mean Accuracy (Cross-validation): {mean_accuracy}')

correct_predictions = np.sum((y_pred == y_test).all(axis=1))

class_counts = np.sum(y_pred, axis=0)


labels = ['Severity_None', 'Severity_Severe']
x = np.arange(len(labels))

plt.bar(x, class_counts)
plt.xticks(x, labels)
plt.ylabel('Number of Predictions')
plt.title('Number of Predictions by Class')
plt.show()
